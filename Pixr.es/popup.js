if (localStorage.domain) {
  ifrm = document.createElement('iframe');
  ifrm.setAttribute('src', 'http://' +  localStorage.domain + '/history');
  ifrm.setAttribute('style', 'width: 100%; height: 100%');
  ifrm.setAttribute('frameborder', 0);
  ifrm.setAttribute('name', 'PixrEsFrame');
  document.body.appendChild(ifrm);
}

document.addEventListener('DOMContentLoaded', function () {
  var domainButton = document.getElementById("domain"),
      changeDomainSection = document.getElementById("changeDomain"),
      domainInput = document.getElementById("domainInput"),
      domainFooter = document.getElementById("footer"),
      domainInputButtonSave = document.getElementById("domainSave");

  domainInput.value = (localStorage.domain) ? localStorage.domain : '';
  domainButton.addEventListener('click', function(){
    changeDomainSection.style.display = 'block';
    domainButton.style.display = 'none';
    domainFooter.style.opacity = 1;
  });

  domainInputButtonSave.addEventListener('click', function(){
    var input = domainInput.value;
    if (input.length > 0) {
      input = (input.substr(-1) == '/') ? input.substr(0, input.length - 1) : input;
      localStorage.domain = input;
      changeDomainSection.style.display = 'none';
      domainButton.style.display = 'block';
      domainFooter.style.opacity = 0.5;
      window.close();
    }
  });
});
